import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.image.*;
import com.sun.j3d.utils.behaviors.vp.*;
import com.sun.j3d.loaders.*;
import com.sun.j3d.loaders.objectfile.*;
import javax.swing.JFrame;
import javax.media.j3d.Light.*;
import java.util.ArrayList;

/**
 *
 * @author Felipe Borges
 */
public class HeartOfGold extends JFrame {
    private Canvas3D canvas3d;
    private BranchGroup scene;
    private SimpleUniverse universe;
    private BranchGroup lights;
    private BoundingSphere bounds;
    private ArrayList<Star> stars;

    public HeartOfGold() {
        this.canvas3d = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
        this.universe = new SimpleUniverse(this.canvas3d);
        this.scene = new BranchGroup();

        this.universe.getViewingPlatform().setNominalViewingTransform();

        this.createSceneGraph();
    }

    public void enableNavigation() {
        OrbitBehavior ob = new OrbitBehavior(this.canvas3d);
        ob.setSchedulingBounds(new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE));

        this.universe.getViewingPlatform().setViewPlatformBehavior(ob);
    }

    private void loadBackground() {
        TextureLoader texture = new TextureLoader("bg.jpg", null);
        Background bg = new Background(texture.getImage());
        bg.setApplicationBounds(this.bounds);

        this.scene.addChild(bg);
    }

    private void loadSpaceship() {
        Spaceship spaceship = new Spaceship();

        this.scene.addChild(spaceship);
    }

    private void addDirectionalLight() {
        DirectionalLight light1 = new DirectionalLight(new Color3f(1.0f, 1.0f, 1.0f),
                                                       new Vector3f(-1.0f, -1.0f, -1.0f));
        light1.setInfluencingBounds(this.bounds);

        this.lights.addChild(light1);
    }

    private void addAmbientLight() {
        AmbientLight ambLight = new AmbientLight(new Color3f(0.5f, 0.5f, 0.5f));
        ambLight.setInfluencingBounds(this.bounds);

        this.lights.addChild(ambLight);
    }

    public void addStars() {
        this.stars = new ArrayList<Star>();

        Star sun = new Star(Star.SUN, new Vector3f(-2.5f, -0.007f, 0.0f), Star.STATIC, 0.4f);
        this.stars.add(sun);

        Star sun2 = new Star(Star.SMALL, new Vector3f(0.2f, 0.3f, 0.8f), Star.STATIC, 0.4f);
        this.stars.add(sun2);

        Star sun3 = new Star(Star.SUN, new Vector3f(0.6f, -0.3f, -0.2f), Star.STATIC, 0.4f);
        this.stars.add(sun3);

        Star sun4 = new Star(Star.SUN, new Vector3f(0.3f, -0.3f, -1.3f), Star.STATIC, 0.4f);
        this.stars.add(sun4);

        Star sun5 = new Star(Star.SUN, new Vector3f(0.1f, -0.7f, -0.3f), Star.STATIC, 0.4f);
        this.stars.add(sun5);

        Star syrius = new Star(Star.SYRIUS, new Vector3f(0.9f, 0.2f, -1.6f), Star.MOVE, Star.LIGHT_INTENSITY_DEFAULT);
        this.stars.add(syrius);
    }

    public void addLight() {
        this.lights = new BranchGroup();

        this.addDirectionalLight();
        this.addAmbientLight();

        for (Star star : this.stars) {
            this.lights.addChild(star.light);

            this.scene.addChild(star);
        }

        this.universe.addBranchGraph(this.lights);
    }

    public void createSceneGraph() {
        this.bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), Double.MAX_VALUE);

        this.loadBackground();
        this.loadSpaceship();
        this.addStars();

        this.addLight();

        this.scene.compile();

        this.universe.addBranchGraph(this.scene);
    }

    public void show_all() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setTitle("Coração de Ouro");
        this.setSize(1000, 700);
        this.getContentPane().add("Center", this.canvas3d);
        this.setVisible(true);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    public static void main(String[] args) {
        HeartOfGold hog = new HeartOfGold();

        //hog.enableNavigation();
        hog.show_all();
    }
}
