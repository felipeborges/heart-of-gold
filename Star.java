import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.image.*;
import com.sun.j3d.utils.behaviors.vp.*;
import com.sun.j3d.loaders.*;
import com.sun.j3d.loaders.objectfile.*;
import javax.swing.JFrame;
import javax.media.j3d.Light.*;

public class Star extends TransformGroup {
    public static final float SMALL = 0.003f;
    public static final float SUN = 0.007f;
    public static final float SYRIUS = 0.02f;
    public static final float ARCTURUS = 0.03f;
    public static final float RYGEL = 0.4f;
    public static final float BETELGEUSE = 0.5f;

    public static final double LIGHT_INTENSITY_DEFAULT = Double.MAX_VALUE;

    public static final boolean MOVE = true;
    public static final boolean STATIC = false;

    private BranchGroup scene;
    public BranchGroup light;
    public float radius;
    private Sphere sphere;
    private TransformGroup lightAndMove;
    private boolean move;
    private double lightIntensity;

    public Star(float radius, Vector3f position, boolean move, double lightIntensity) {
        this.radius = radius;
        this.move = move;
        this.lightIntensity = lightIntensity;

        this.scene = new BranchGroup();

        //Generate an Appearance.
        Color3f ambientColourShaded = new Color3f(0.0f,0.4f,0.4f);
        Color3f emissiveColourShaded = new Color3f(0.0f,0.0f,0.0f);
        Color3f diffuseColourShaded = new Color3f(0.9f, 0.8f, 0.1f);
        Color3f specularColourShaded = new Color3f(0.0f,0.5f,0.5f);

        float shininessShaded = 2.0f;

        Appearance shadedApp = new Appearance();
        shadedApp.setMaterial(new Material(ambientColourShaded, emissiveColourShaded,
                                           diffuseColourShaded, specularColourShaded, shininessShaded));

        this.sphere = new Sphere(this.radius, Sphere.GENERATE_NORMALS, 50, shadedApp);
        Transform3D tfSphere = new Transform3D();
        tfSphere.setTranslation(position);
        this.setTransform(tfSphere);

        this.addChild(this.sphere);

        this.addLight();
    }

    public void addLight() {
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), this.lightIntensity);

        PointLight lightSpot = new PointLight(new Color3f(0.5f, 0.4f, 0.1f),
                                              new Point3f(0.0f,0.0f,0.7f),
                                              new Point3f(0.05f,0.05f,0.01f));

        lightSpot.setInfluencingBounds(bounds);

        if (this.move)
            this.move(bounds);

        this.addChild(lightSpot);
    }

    public void move(BoundingSphere bounds) {
        Alpha alphaLight = new Alpha(-1, 20000);
        Transform3D axisOfTranslation = new Transform3D();
        axisOfTranslation.rotY(0);
        PositionInterpolator inter = new PositionInterpolator(alphaLight, this,
                                                              axisOfTranslation, -8.0f, 8.5f);

        inter.setSchedulingBounds(bounds);
        this.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        this.addChild(inter);
    }
}
