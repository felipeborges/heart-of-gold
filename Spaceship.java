import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.image.*;
import com.sun.j3d.utils.behaviors.vp.*;
import com.sun.j3d.loaders.*;
import com.sun.j3d.loaders.objectfile.*;
import java.util.Hashtable;
import java.util.Enumeration;

public class Spaceship extends TransformGroup {
    private Scene scene;
    private ObjectFile file;

    public Spaceship() {
        this.file = new ObjectFile(ObjectFile.RESIZE);
        this.scene = null;

        this.loadObj();
        this.setDefaultPosition();
        //this.loadTextures();
        this.move();
    }

    private void loadObj() {
        try {
            this.scene = this.file.load("discovery.obj");
        } catch (Exception e) {
            System.out.println("File loading failed: " + e);
        }
    }

    private void setDefaultPosition() {
        Transform3D tfObject = new Transform3D();
        Transform3D xRotation = new Transform3D();
        Transform3D yRotation = new Transform3D();

        tfObject.rotZ(Math.PI/5);
        xRotation.rotX(-Math.PI/13);
        yRotation.rotY(-Math.PI/6);

        tfObject.setTranslation(new Vector3f(0.05f, 0.05f, 0.8f));
        tfObject.mul(yRotation);
        tfObject.mul(xRotation);

        this.setTransform(tfObject);
        this.addChild(this.scene.getSceneGroup());
    }

    public void getPartsOfSpaceship() {
        Hashtable namedObjects = this.scene.getNamedObjects();
        Enumeration enumer = namedObjects.keys();
        String name;
        while (enumer.hasMoreElements()) {
            name = (String) enumer.nextElement();
            System.out.println("Name: "+name);
        }
    }

    public void move() {
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), Double.MAX_VALUE);
        Alpha alphaLight = new Alpha(-1, 100000);
        Transform3D axisOfTranslation = new Transform3D();
        float[] knots = new float[3];
        knots[0] = 0.0f;
        knots[1] = 0.2f;
        knots[2] = 1.0f;
        Point3f[] positions = new Point3f[3];
        positions[0] = new Point3f(0.0f, -0.5f, -2.0f);
        positions[1] = new Point3f(-1.0f, 0.5f, -0.3f);
        positions[2] = new Point3f(0.5f, 0.2f, 0.3f);
        PositionPathInterpolator inter = new PositionPathInterpolator(alphaLight, this,
                                                                      axisOfTranslation, knots, positions);

        inter.setSchedulingBounds(bounds);
        this.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        this.addChild(inter);
    }
}
